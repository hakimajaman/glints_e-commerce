const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const helper = require('../helper/helper.customer');

let usersSchema = require('../models/models.users');
let shopSchema = require('../models/model.shop');

let RegisterUser = async (req, res) => {
    try {
        let post = await usersSchema.registSchema.create({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: req.body.password,
            datebirth: req.body.datebirth,
            address: req.body.address,
            gender: req.body.gender,
            isMerchant: req.body.isMerchant
        });
        let findEmail = await usersSchema.registSchema.find()

        post.password = await bcrypt.hashSync(post.password, saltRounds = 10)
        post.save()
        res.status(201).json(helper.successresponse(post, "Registering Succesed"));
    }
    catch (err) {
        res.status(400).json(helper.errorresponse(err, "Error brader"))
    }
}

let LoginUser = async (req, res) => {
    const {
        email
    } = req.body;

    const UserExists = await usersSchema.registSchema.findOne({
        email
    })

    if(req.body.password && !UserExists){
        res.json("Invalid Email or Password");
        return console.log("Invalid Email or Password");
    }

    bcrypt.compare(req.body.password, UserExists.password, async (error, success) => {
        if (success) {
            const token = jwt.sign({
                id: UserExists._id,
                isMerchant: UserExists.isMerchant
            }, 'LoginToken', {
                    algorithm: 'HS256',
                    expiresIn: '12h'
                });

            res.setHeader('Authorization', `Bearer ${token}`);

            await usersSchema.registSchema.findByIdAndUpdate({
                _id: UserExists.id
            }, {
                    $set: {
                        isChanged: true
                    }
                });

            res.status(200).json({
                Success: true,
                Token: token,
                rolebased: UserExists.rolebased
            });
            return console.log("Login Successful");
        }
        res.json("Invalid Email or Password");
        return console.log("Invalid Email or Password");
    })
}

var idcart = [];

let userCart = async (req, res) => {

    var decoded = jwt.verify(req.headers.authorization, 'LoginToken');

    // try{
        await usersSchema.registSchema.findById(decoded.id)
        .populate({
            path: 'buy',
            populate: {
                path: 'detail',
                select: 'name price -_id'
            },
            select: 'cart detail totalprice'
        })
        .select('buy -_id')
        .exec()
        .then((showCart) => {
            var result = helper.usercartHelper(showCart)
            result.forEach(cart => {
                idcart.push(cart.id)
            })
            console.log(idcart)
            res.status(200).json(helper.successresponse(result, "Success show user cart"))
        })
    // }
    // catch(err){
    //     res.status(400).json(helper.errorresponse(err, "Error Brader, Login dulu"))
    // }
}


let userBuying = async (req, res) => {

    var decoded = jwt.verify(req.headers.authorization, 'LoginToken')
    
    // try {
        
        let findProductBuy = await usersSchema.registSchema.findById(decoded.id)
        // if (!findProductBuy) {
        //     return res.send('There is no account')
        // }

        var a = await findProductBuy.buy
        var cart = req.params.idcart
        
        if (a.indexOf(cart) == -1 ) {
            return res.status(200).json('There is no product have been you carted')
        }

        var testfindcart = await shopSchema.cartsSchema.findById(cart)
            .populate({
                path: 'detail'
            })
        var qtyy = testfindcart.detail[0].id
        var minqty = testfindcart.detail[0].qty - testfindcart.cart
        await shopSchema.shoppSchema.findByIdAndUpdate(qtyy, {$set: {qty: minqty}}, {isChanged: true})
        await shopSchema.cartsSchema.findById(cart)
        .populate({
            path: 'detail'
        })
        
        var sendtoHistory = {
            cart: testfindcart.cart,
            totalprice: testfindcart.totalprice,
            detail: testfindcart.detail
        }

        var receiveHistory = await usersSchema.histori.create(sendtoHistory)
        var thisis = await usersSchema.registSchema.findById(decoded.id)
        thisis.history.push(receiveHistory)
        await thisis.save();
        var deletecart = await shopSchema.cartsSchema.findByIdAndDelete(cart)

        var findcartAgain = await shopSchema.cartsSchema.find()

        res.status(200).json(helper.successresponse(findcartAgain, "Buying Success"))
    // }
//     catch (err) {
//         console.log(err)
//         res.status(400).json(helper.errorresponse(err, "Error Brader"))
//     }
}

let userHistory = async (req, res) => {

    var decoded = jwt.verify(req.headers.authorization, 'LoginToken');
    // try{
        let findHistory = await usersSchema.registSchema.findById(decoded.id)
            .populate({
                path: 'history'
            }).select('history')
        res.json(helper.successresponse(findHistory, "Success show user history"))

    // }
    // catch(err){
    //     res.json(helper.errorresponse(err, "Error Brader"))
    // }

}

module.exports = {
    RegisterUser,
    LoginUser,
    userCart,
    userBuying,
    userHistory,
    // cart
}
