const shopSchema = require('../models/model.shop');
const usersSchema = require('../models/models.users').registSchema

const helper = require('../helper/helper.customer');
const multer = require('multer')

let jwt = require('jsonwebtoken');

var upload = multer().single('img')

var ImageKit = require('imagekit');
require('dotenv')
var imagekit = new ImageKit({
    "imagekitId": 'hakimajaman',
    "apiKey": 'Fu5bvMd2PPsQV3sV/HOUHvoQS3s=',
    "apiSecret": 'eulQF6yB7OZzxNY1fTXwvzRtSAQ='
});

let postSale = async (req, res) => {
    var decoded = jwt.verify(req.headers.authorization, 'LoginToken')

    upload(req, res, async (err) => {
        var file = req.file

        if (!file) {
            return res.status(422).json("please upload file")
        }

        var linkimage = await imagekit.upload(req.file.buffer.toString('base64'), {
            "filename": req.file.originalname,
            "folder": "/product"
        })

        var image = linkimage.url

        let post = shopSchema.shoppSchema.create({
            name: req.body.name,
            price: req.body.price,
            qty: req.body.qty,
            description: req.body.description,
            category: req.body.category,
            image: image
        }).then(save => {

            usersSchema.findOneAndUpdate({ _id: decoded.id }, { $push: { shop: save._id } })
            .then(result => {
                    res.status(201).json(helper.successresponse(result, "Your Product have been post!"));

                })

                // user.shop.push(post);
                // let result = await user.save();
                // res.status(201).json(helper.successresponse(user, "Your Product have been post!"));
        })



    })
}

let updateSale = async (req, res) => {
    try {
        var decoded = jwt.verify(req.headers.authorization, 'LoginToken')
        let updateit = await shopSchema.shoppSchema.findByIdAndUpdate(req.params.id, {
            $set: req.body
        })
        let showthesale = await usersSchema.findById(decoded.id)
            .populate('shop').select('shop')
        res.status(200).json(helper.successresponse(showthesale, "Success update a sale"))
    }
    catch (err) {
        res.status(400).json(helper.errorresponse(err, "Error Brader"))
    }
}

let deleteSale = async (req, res) => {
    var decoded = jwt.verify(req.headers.authorization, 'LoginToken')
    let deleteit = await shopSchema.shoppSchema.findByIdAndDelete(req.params.id)
    let showthesale = await usersSchema.findById(decoded.id)
        .populate('shop').select('shop')
    res.status(200).json(helper.successresponse(showthesale, "Success delete a sale"))
}

let allSale = async (req, res) => {
    let index = await shopSchema.shoppSchema.find()
        .select('name price qty image description')
    res.status(200).json(helper.successresponse(index, "Success show all sale"))
}

let customerCart = async (req, res) => {

    var decoded = jwt.verify(req.headers.authorization, 'LoginToken')
    let productCart = ({
        cart: req.body.cart
    })

    try {
        let productExists = await shopSchema.shoppSchema.findById(req.params.id);        // 1) Params product id for link;
        if (!productExists) {                                                                //if the product is nothing, then ~
            return res.json("Sorry,the product you are looking for was not found")
        }

        let user = await usersSchema.findById(decoded.id);                      // 2) Find user by auth
        let post = await shopSchema.cartsSchema.create(productCart);            // 3) create the qty of cart user
        
        var idcart = post.id                                                        //save the id that have been created
        if(post.cart > productExists.qty){
            await shopSchema.cartsSchema.findByIdAndDelete(idcart)
            return res.json('Belinya kebanyakan, emang ga liat Quantitynya berapa?')
        }
        let cart = await shopSchema.cartsSchema.findById(idcart);               // 4) Find the new cart that have been created by idcart that have been saved before

        let productCartt = await productExists                                  // 6) Prepare it for push
        user.buy.push(post);                                                    // 7) Push a cartSchema to user buy for populate;
        cart.detail.push(productCartt);                                         // 8) and Push a product that have been findbyid to detail of a new cart;
        let equalCart = cart.cart * cart.detail[0].price
        
        await shopSchema.cartsSchema.findByIdAndUpdate(idcart, { $set: { totalprice: equalCart } }); // 5) Set the total price of cart by update it from the shop schema
        // cart.totalprice.push(equalCart)
        await cart.save();                                                      // 9 & 10 ) and Save it
        await user.save();
        //when it was saved, all of schema have been updated,
        let cart2 = await shopSchema.cartsSchema.findById(idcart)               //11) show it a new cart, that's has been updated
            .populate({
                path: 'detail',
                select: 'name price description'
            })
            .select('cart totalprice')

        res.json(helper.successresponse(cart2, "Succes show the cart"));
    }
    catch (err) {
        res.status(400).json(helper.errorresponse(err, "Error Brader"))
    }


}

module.exports = {
    //penjual // sales
    postSale,
    deleteSale,
    updateSale,

    //umum // general
    allSale,

    //pembeli //buyer
    customerCart
}
