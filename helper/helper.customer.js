const userController = require('../controllers/controllers.users')

const successresponse = function(result, messages){
    return{
        success: true,
        messages: messages,
        result: result
    }
}

const errorresponse = function(result, messages){
    return{
        success: false,
        messages: messages,
        result: result
    }
}

let usercartHelper = (args) => {

    let array = [];

    let buy = args.buy

    for(var i = 0; i < buy.length; i++){
        let name = buy[i].detail[0].name
        let cart = buy[i].cart
        let total = buy[i].totalprice
        let id = buy[i].id

        var data = {
            name: name,
            cart: cart,
            total_price: total,
            id: id
        }
        array.push(data)
    }
    return array
}

// let cartHelper = (args) => {

//     let cart = args

//     var data = {
//         detail: cart.detail[0],
//         total_price: cart.totalprice
//     }

//     return data
// }

// let buyingHelper = (args) => {

//     let array = [];
//     let cart = args.buy

//     for(var i = 0; i < cart.length; i++){
//         cart[i]
//         array.push(cart)
//     }
//     return array
// }

// let userCartArray = (args) => {

//     let array = [];
//     let cart = args.result

//     for(var i=0; i<cart; i++){
//         var idcart = cart[i].id
//         array.push(idcart)
//     }
//     return array
// }


module.exports = {
    successresponse,
    errorresponse,
    usercartHelper,
    // cartHelper,
    // buyingHelper,
    // userCartArray
}