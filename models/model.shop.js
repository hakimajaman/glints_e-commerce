const mongoose = require('mongoose');

const shopSchema = new mongoose.Schema({
    name: {
        type: String,
        min: 3,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    qty: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    }
})

const cartSchema = new mongoose.Schema({
    cart: {
        type: Number
    },
    totalprice: {
        type: Number
    },
    detail: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Shop'
    }]
})

const shoppSchema = mongoose.model('Shop', shopSchema)
const cartsSchema = mongoose.model('Cart', cartSchema)
module.exports = {
    shoppSchema,
    cartsSchema
}