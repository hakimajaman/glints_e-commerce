const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const mail = require('mongoose-type-email');

const registerSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true,
        min: 3
    },
    lastname: {
        type: String,
        required: true,
        min: 3
    },
    email: {
        type: mail,
        required: [true, 'email must be filled'],
        unique: [true, 'email already exist, must be unique']
    },
    password: {
        type: String,
        required: true,
        minlength: [5, 'Too short, min 5 character']
    },
    datebirth: {
        type: Date,
        required: true
    },
    gender: {
        type: String,
        enum: ['male', 'female'],
        required: true
    },
    address: {
        type: String,
        required: true
    },
    isMerchant: {
        type: Boolean,
        default: false,
    },
    shop: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Shop'
    }],
    buy: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Cart'
    }],
    history: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'History'
    }]
})

const history = new mongoose.Schema({
    cart: {
        type: Number
    },
    totalprice: {
        type: Number
    },
    detail: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Shop'
    }]
})

const registSchema = mongoose.model('RegisterUsers', registerSchema)
const histori = mongoose.model('History', history)
module.exports = {
    registSchema,
    histori
}