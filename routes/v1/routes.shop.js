var express = require('express');
var router = express.Router();
var shopcontroller = require('../../controllers/controller.shop')

//auth
var auth = require('../../middleware/auth')
var authoriz = require('../../middleware/authRoleBased')

/* GET home page. */
router.get('/', shopcontroller.allSale);

//merchant
router.post('/postshop', [auth, authoriz.roleBased], shopcontroller.postSale)
router.delete('/delete/:id', auth, shopcontroller.deleteSale)
router.put('/update/:id', auth, shopcontroller.updateSale)


//customer
router.post('/buying/:id', auth, shopcontroller.customerCart)
// router.get('/buying/myCart/', auth, shopcontroller.buying)

module.exports = router;
