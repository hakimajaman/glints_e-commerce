var express = require('express');
var router = express.Router();
var userscontroller = require('../../controllers/controllers.users');
var auth = require('../../middleware/auth');

router.post('/register', userscontroller.RegisterUser)
router.post('/login', userscontroller.LoginUser)
router.post('/cart/buying/myCart/:idcart', auth, userscontroller.userBuying)

//user get
router.get('/cart', auth, userscontroller.userCart)
router.get('/history', auth, userscontroller.userHistory)

module.exports = router;
