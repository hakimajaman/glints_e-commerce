var express = require('express');
var router = express.Router();

//routeV1
var usersRouter = require('./v1/routes.users')
var shopRouter = require('./v1/routes.shop')

/* GET home page. */

router.use('/users', usersRouter)
router.use('/shop', shopRouter)

module.exports = router;
