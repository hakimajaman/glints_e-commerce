let conf;
let mongoose = require('mongoose');
// let env = process.env.NODE_ENV = 'development';

// require('dotenv').config()

// if (env) {
//     conf = {
//         uri: process.env.DBDEV
//     }
// }


const env = process.env.NODE_ENV

/* istanbul ignore else */
if(env == 'development' || env == 'test'){
    require('dotenv').config()
}

const configDB = {
    development: process.env.DBDEV,
    test: process.env.DBTEST,
    production: process.env.DBPROD
}

//mongoose connection
mongoose.connect(configDB[env], {
    useNewUrlParser: true,
    useFindAndModify: false
}).then(() => console.log("Connected to Database"))

mongoose.Promise = global.Promise;