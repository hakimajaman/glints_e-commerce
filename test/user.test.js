const server = require('../app');
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect()
const should = chai.should()
const fs = require('fs')


chai.use(chaiHttp);

var theMerchant = {
    firstname: 'Integration',
    lastname: 'Test',
    email: 'integration@test.com',
    password: 'password',
    datebirth: 2019-09-18,
    gender: 'male',
    address: 'GLINTSxBinar',
    isMerchant: true
}
var theUsers = {
    firstname: 'Users-Integration',
    lastname: 'Buyers',
    email: 'Buyers@test.com',
    password: 'password',
    datebirth: 2019-09-18,
    gender: 'male',
    address: 'BinarXGLINTS',
    isMerchant: false
}

var theMerchantToken
var theUsersToken
var idProduct
var idCart
var fakeToken = 'abc'
var filePict = './public/images/400-badRequest.png'

//////////////////////////////////////////// ------------POSITIVE TESTING

describe('/POST to register users', ()=> {
    it('it should create a new users', done => {
        chai.request(server)
            .post('/api/users/register')
            .send(theMerchant)
            .end((err, res)=> {
                if(err){
                    console.log(err)
                }
                res.should.have.status(201);
                res.body.should.be.an('object')
                done();
            });
    });
});

describe('/POST to register users', ()=> {
    it('it should create a new users', done => {
        chai.request(server)
            .post('/api/users/register')
            .send(theUsers)
            .end((err, res)=> {
                if(err){
                    console.log(err)
                }
                res.should.have.status(201);
                res.body.should.be.an('object')
                done();
            });
    });
});

describe('/POST to Login users', ()=> {
    it('it should login a users', done => {
        chai.request(server)
            .post('/api/users/login')
            .send({
                email: theMerchant.email,
                password: theMerchant.password
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                theMerchantToken = res.body.Token
                res.should.have.status(200);
                res.body.should.be.an('object')
                done()
            })
    })

})

describe('/POST to Login users', ()=> {
    it('it should login a users', done => {
        chai.request(server)
            .post('/api/users/login')
            .send({
                email: theUsers.email,
                password: theUsers.password
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                theUsersToken = res.body.Token
                // console.log(theUsersToken)
                res.should.have.status(200);
                res.body.should.be.an('object')
                done()
            })
    })
})

///////////////////////////// ------------Shop Testing
describe('/POST to post a product', ()=> {
    it('it should post a product', done => {
        chai.request(server)
            .post('/api/shop/postshop')
            .field({
                name: 'Heberal MRojax',
                price: 700000,
                qty: 100,
                description: 'Salep anti galau',
                category: 'Obat-obatan'
            }) 
            .attach('img', fs.readFileSync(`${filePict}`), '400-badRequest.png')
            .set('authorization', theMerchantToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(201);
                res.body.should.be.an('object')
                done()
            })

    })
})

describe('/GET to show all product', () => {
    it('it should show all product sale', done => {
        chai.request(server)
            .get('/api/shop/')
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                idProduct = res.body.result[0]._id
                res.should.have.status(200);
                res.body.should.be.an('object')
                done()
            })
    })
})

describe('/PUT to update a product', () => {
    it('it should update a product by id', done => {
        chai.request(server)
            .put('/api/shop/update/' + idProduct)
            .set('authorization', theMerchantToken)
            .send({
                // name: 'Heberal MRojax',
                price: 600000,
                // qty: 100,
                // description: 'Salep anti galau',
                // category: 'Obat-obatan'
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200)
                res.body.should.be.an('object')
                done()
            })
    })
})

///////////////////////////// ------------Buyers Testing
describe('/POST to put a product to cart', () => {
    it('it should put a product to cart', done => {
        chai.request(server)
            .post('/api/shop/buying/' + idProduct)
            .set('authorization', theUsersToken)
            .send({
                cart: 9
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200)
                res.body.should.be.an('object')
                done()
            })
    })
    it('it should fail to put a product to cart because the quantity of product is less than tha user wants', done => {
        chai.request(server)
            .post('/api/shop/buying/' + idProduct)
            .set('authorization', theUsersToken)
            .send({
                cart: 10000
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200)
                res.body.should.be.an('string')
                done()
            })
    })
})

describe('/GET to show all buyer cart', () => {
    it('it should show all buyer cart', done => {
        chai.request(server)
            .get('/api/users/cart')
            .set('authorization', theUsersToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                idCart = res.body.result[0].id
                console.log(idCart)
                res.should.have.status(200)
                res.body.should.be.an('object')
                done()
            })
    })
})

describe('/POST to buy a product from the cart by user', () => {
    it('it should buy a product from the cart', done => {
        chai.request(server)
            .post('/api/users/cart/buying/myCart/' + idCart)
            .set('authorization', theUsersToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200)
                res.body.should.be.an('object')
                done()
            })
    })
})

describe('/GET to show all histories of user has buyed', () => {
    it('it should show all histories', done => {
        chai.request(server)
            .get('/api/users/history')
            .set('authorization', theUsersToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200)
                res.body.should.be.an('object')
                done()
            })
    })
})

/* POSITIVE TESTING------------ //////////////////////////////////////////// */

//////////////////////////////////////////// ------------NEGATIVE TESTING

describe('/POST to fail register users', ()=> {
    it('it should fail for create a new users', done => {
        chai.request(server)
            .post('/api/users/register')
            .send({
                firstname: 'Integration',
                lastname: 'Test',
                datebirth: 2019-09-18,
                gender: 'male',
                address: 'GLINTSxBinar',
                isMerchant: true
            })
            .end((err, res)=> {
                if(err){
                    console.log(err)
                }
                res.should.have.status(400);
                res.body.should.be.an('object')
                done();
            });
    });
});

describe('/POST to fail login users', () => {
    it('it should invalid email or password because no email', done => {
        chai.request(server)
            .post('/api/users/login')
            .send({
                email: '',
                password: theUsers.password
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200);
                res.body.should.be.an('string')
                done();
            });
    });
    it('it should invalid email or password because no password', done => {
        chai.request(server)
            .post('/api/users/login')
            .send({
                email: theUsers.email,
                password: ''
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200);
                res.body.should.be.an('string')
                done();
            });
    });
    it('it should invalid email or password because wrong input it', done => {
        chai.request(server)
            .post('/api/users/login')
            .send({
                email: theUsers.email,
                password: 'wrongit'
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200);
                res.body.should.be.an('string')
                done();
            });
    });
    it('it should invalid email or password because wrong input it', done => {
        chai.request(server)
            .post('/api/users/login')
            .send({
                email: 'wrongit@mail.com',
                password: theUsers.email
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200);
                res.body.should.be.an('string')
                done();
            });
    });
});

describe('/GET all buyer cart to fail authentication', () => {
    it('it should fail to authenticate because the token was not found', done => {
        chai.request(server)
            .get('/api/users/cart')
            .set('authorization', fakeToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(403)
                res.body.should.be.an('object')
                done();
            });
    });
    it('it should fail to authenticate because the token was not input', done => {
        chai.request(server)
            .get('/api/users/cart')
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(403)
                res.body.should.be.an('object')
                done()
            })
    })
})

describe('/GET to badRequest', () => {
    it('it should fail because there is no endpoint of it', done => {
        chai.request(server)
            .get('/api/users/badrequest')
            .set('authorization', fakeToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(404)
                done();
            });
    });
    it('it should fail to authenticate because the token was not input', done => {
        chai.request(server)
            .get('/api/users/cart')
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(403)
                res.body.should.be.an('object')
                done()
            })
    })
})

///////////////////////////// ------------Shop Testing

describe('/POST to fail post a product', ()=> {
    it('it should fail to post a product because its not merchant', done => {
        chai.request(server)
            .post('/api/shop/postshop')
            .field({
                name: 'Heberal MRojax',
                price: 700000,
                qty: 100,
                description: 'Salep anti galau',
                category: 'Obat-obatan'
            })
            .attach('img', fs.readFileSync(`${filePict}`), '400-badRequest.png')
            .set('authorization', theUsersToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(403);
                res.body.should.be.an('string')
                done();
            });
    });
    it('it should fail for post a product because there is no picture to upload', done => {
        chai.request(server)
            .post('/api/shop/postshop')
            .field({
                name: 'Heberal MRojax',
                price: 700000,
                qty: 100,
                description: 'Salep anti galau',
                category: 'Obat-obatan'
            })
            .set('authorization', theMerchantToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(422)
                res.body.should.be.an('string')
                done();
            });
    });
});

///////////////////////////// ------------Buyers Testing
describe('/POST to fail for buying a product', () => {
    it('it should fail for buying a product because it wasnt it cart', done => {
        chai.request(server)
            .post('/api/users/cart/buying/myCart/' + 'badIdProduct')
            .set('authorization', theUsersToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200);
                res.body.should.be.an('string')
                done();
            });
    });
});

describe('/POST to fail put a product to cart', () => {
    it('it should fail to put a product to cart because the id product is nothing', done => {
        chai.request(server)
            .post('/api/shop/buying/' + 'badIdProduct')
            .set('authorization', theUsersToken)
            .send({
                cart: 9
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200)
                res.body.should.be.an('string')
                done()
            })
    })
    it('it should fail to put a product to cart because cart is empty', done => {
        chai.request(server)
            .post('/api/shop/buying/' + idProduct)
            .set('authorization', theUsersToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(400)
                res.body.should.be.an('object')
                done()
            })
    })
})

describe('/PUT to fail update a product', () => {
    it('it should fail update a product by id because wrong type input', done => {
        chai.request(server)
            .put('/api/shop/update/' + idProduct)
            .set('authorization', theMerchantToken)
            .send({
                price: 'six thousand'
            })
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(400)
                res.body.should.be.an('object')
                done()
            })
    })
})

/* NEGATIVE TESTING------------ //////////////////////////////////////////// */

/* it delete Shop Testing */

describe('/DELTE to delete a product', () => {
    it('it should delete a product by id', done => {
        chai.request(server)
            .delete('/api/shop/delete/' + idProduct)
            .set('authorization', theMerchantToken)
            .end((err, res) => {
                if(err){
                    console.log(err)
                }
                res.should.have.status(200)
                res.body.should.be.an('object')
                done()
            })
    })
})


module.exports = {
    theMerchant,
    theUsers,
    theMerchantToken,
    theUsersToken
}