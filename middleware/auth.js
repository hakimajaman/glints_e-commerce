const jwt = require('jsonwebtoken');

let isAuthenticated = (req,res,next) => {
    var token = req.headers.authorization;
    if(token){
        jwt.verify(token, 'LoginToken', (err, decoded) => {
            if(err){
                res.status(403).json({ Message: "Failed to Authenticate"})
            }
            else{
                req.user = decoded;
                next();
            }
        });
    } else{
        return res.status(403).json({Message: 'No token provided'});
    }
}

module.exports = isAuthenticated