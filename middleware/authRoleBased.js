let roleBased = function (req, res, next) {
    if(!req.user.isMerchant){
        res.status(403).json('Access Denied')
        return
    }
    next()
}

module.exports = {
    roleBased
}